"use strict";
//API KEY do Forecast ede77008568d355ca28d93f10f5681d6
// https://api.forecast.io/forecast/e0a48365d3cdf6feea525c85ca579ea7/-23.5513683,-46.634284,2014-05-06T00:00:00-0200?units=si
var Forecast = require("forecast.io-bluebird");
var moment = require("moment-timezone");
var exporter = require("datastore-exporter");
var path = require("path");
var fs = require("fs");
var NodeZip = require('node-zip');
var timestampArray = require("./timestampsSortedNonRepeated.json");

var zip = new NodeZip();
// var zip = new NodeZip("weatherCache.zip");

var forecast = new Forecast({
    key: "e0a48365d3cdf6feea525c85ca579ea7",
    // timeout: 2500
});

var latitude = -23.5513683, longitude = -46.634284; // Catedral da Sé

// var zipEntries = zip.getEntries();
// var stringified = JSON.stringify(zipEntries[0]);
// console.log(typeof(stringified), stringified);

var stringified = JSON.stringify(timestampArray);
// console.log(typeof(stringified), stringified);
var startDate = new moment.tz([2012,0,1], "America/Sao_Paulo");
// var endDate = new moment.tz([2016,0,1], "America/Sao_Paulo");
// zip.addFile(startDate.clone().utc().format("YYYYMMDD[.txt]"), stringified, "", 32);
// zip.writeZip("weatherCache.zip");

// zip.file(startDate.clone().utc().format("YYYYMMDD[.txt]"), stringified);
// var data = zip.generate({base64:false,compression:'DEFLATE'});
// fs.writeFileSync('weatherCache.zip', data, 'binary');

var options = {
    units: "si",
    // exclude: "hourly,minitely",
    // exclude: "minutely",
    // lang: "x-pig-latin"
    lang: "en"
};

var hierarchy = path.join("weather", "interpolationReady", "hourlyTemperaturePrecipitation");
var regex = new RegExp("\\" + path.sep, "g");
var tagName = hierarchy.replace(regex, ".");
var commentary = "temperature:\%fC precipitation:\%fmm/h";
var hourlyArray =[];

// function jsonToDatastore (weatherObj) {
//     var hourlyArray = weatherObj.hourly.data;
//     // var t = new moment.tz(hourlyArray[0], "America/Sao_Paulo");
//     // t.subtract(1, "hour");
//     // exporter.writeToDatastoreLog(hierarchy, t, tagName, [0,0], commentary);
//     hourlyArray.forEach(function (item){
//         var t = new moment.unix(item.time);
//         exporter.writeToDatastoreLog(hierarchy, t, tagName, [item.temperature, item.precipIntensity], commentary);
//     });
//     // t = new moment.tz(hourlyArray[hourlyArray.length-1], "America/Sao_Paulo");
//     // t.add(1, "hour");
//     // exporter.writeToDatastoreLog(hierarchy, t, tagName, [0,0], commentary);
// }

var timestampIndex = 0;
var fetchForecast = function (timestamp, callback) {
    console.log("fetch");
    forecast.fetch(latitude, longitude, timestamp.valueOf(), options)
    .then(function(result) {
        console.log("then");
        if (typeof(callback) === 'function' && timestampIndex < timestampArray.length) {
            console.log("callback 1");
            var t = new moment.tz(timestampArray[timestampIndex++], "America/Sao_Paulo");
            console.log("callback 2");
            t.hour(0).minute(0).second(0);
            console.log("callback 3");
            console.log(t.format());
            zip.file(t.clone().utc().format("YYYYMMDD[.json]"), new Buffer(JSON.stringify(result)));
            console.log("callback 4");
            //TODO: Find out why the array had doubled entries
            hourlyArray = hourlyArray.concat(result.hourly.data);
            console.log("callback 5");
            callback(t, fetchForecast);
        }
        else {
            console.log("callback false");
            var data = zip.generate({base64:false,compression:'DEFLATE'});
            fs.writeFileSync('weatherCache.zip', data, 'binary');
            var lastMoment = moment.unix(hourlyArray[0].time);
            lastMoment.subtract(1, "hour");
            exporter.writeToDatastoreLog(hierarchy, lastMoment, tagName, [0,0], commentary);
            hourlyArray.forEach(function (item) {
                var t = moment.unix(item.time);
                if(t.diff(lastMoment, "hours") > 1) {
                    exporter.writeToDatastoreLog(hierarchy, lastMoment.add(1, "hour"), tagName, [0,0], commentary);
                    exporter.writeToDatastoreLog(hierarchy, t.clone().subtract(1, "hour"), tagName, [0,0], commentary);
                }
                exporter.writeToDatastoreLog(hierarchy, t, tagName, [item.temperature, item.precipIntensity], commentary);
                lastMoment = t.clone();
            });
        }
        //console.dir(result);
    })
    .catch(function(error) {
        console.error("Forecast error:", error);
    });
};

(function () {
    // var t = new moment.tz(timestampArray[timestampIndex++], "America/Sao_Paulo");
    // t.hour(0).minute(0).second(0);
    // fetchForecast(t, fetchForecast);
})();