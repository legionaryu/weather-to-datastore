"use strict";

var path = require("path");
var fs = require("fs");

function getAllLogs(filePath, callback, args) {
    if(!filePath) {
        var result = getAllLogs("./", callback, args);
        return result;
    } else {
        console.log("filePath:", filePath);
        var stat = fs.statSync(filePath);
        if(stat.isDirectory()){
            if(path.basename(filePath)[0] !== ".") {
                var allFiles = fs.readdirSync(filePath);
                allFiles.forEach(function(file){
                    result = getAllLogs(path.join(filePath, file), callback, args);
                });
            }
        } else if (stat.isFile()) {
            if(path.basename(filePath)[0] !== "." && path.extname(filePath) === ".log") {
                if (typeof(callback) === 'function') {
                    args = args instanceof Array ? args : [];
                    args.unshift(filePath);
                    callback.apply(this, args);
                }
            }
        }
    }
}

function validateLogFiles(filePath, fixFiles) {
    fixFiles = fixFiles ? true : false;
    var content = fs.readFileSync(filePath);
    var lines = content.toString().match(/^.*([\n\r]+|$)/gm);
    var finalLines = [];
    var timestamp = 0;
    lines.forEach(function (item) {
        var split = item.split(",", 1);
        // console.log(typeof(split), split);
        var temp_timestamp = Number(split);
        // console.log(typeof(temp_timestamp), temp_timestamp);
        if(temp_timestamp && temp_timestamp > timestamp) {
            timestamp = temp_timestamp;
            // console.log("Valid timestamp order");
            if(fixFiles) {
                var text = item.replace(/[\r\n]/g, "");
                finalLines.push(text);
            }
        }
    });
    // console.log("finalLines.length: %d fixFiles: %s", finalLines.length, fixFiles);
    if(fixFiles && finalLines.length > 0) {
        var textToWrite = finalLines.join("\r\n");
        console.log("Written %d bytes", textToWrite.length);
        fs.writeFileSync(filePath, textToWrite);
    }
}

(function main() {
    var args = process.argv;
    // console.log(args);
    if(args.length > 2 && typeof(args[2]) === "string") {
        getAllLogs(path.normalize(args[2]), validateLogFiles, [true]);
    }
    else {
        console.log("Usage `node datastore-validator.js [folder to scan]`");
    }
})();